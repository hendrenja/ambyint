#include <include/client.h>
#include <iostream>
#include <thread>
#include <signal.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

const char * CONFIG_PORT_KEY = "port";
const char * CONFIG_IP_KEY = "server_ip";

const std::string TCP_SERVER_NAME = "TCP_SERVER";
const uint64_t DEFAULT_SLEEP_US = 20;

/// Execute Thread
std::mutex          m_threadMutex;
std::thread         m_thread;
sig_atomic_t        m_running = false;
sig_atomic_t        m_stopRequest = false;

/// Socket
struct sockaddr_in  m_address;
struct sockaddr_in  m_serverAddress;
int                 m_socket;
int                 m_port = 0;
std::string         m_ip;

#define JSON_VALUE_FREE(ptr) if (ptr != nullptr) { json_value_free(ptr); ptr = nullptr;}
#define JSON_VERIFY_PTR(ptr, name) if (ptr == nullptr) { \
            corto_error("Failed JSON access - Invalid pointer [%s]", name); return false;}

bool UpdateConfig(std::string &configFile, int &port, std::string &ip)
{
    JSON_Value *pFile = json_parse_file(configFile.c_str());
    JSON_VERIFY_PTR(pFile, "pFile");

    JSON_Object *pFileObj = json_value_get_object(pFile);
    JSON_VERIFY_PTR(pFileObj, "pFileObj");

    double rawPort = json_object_get_number(pFileObj, CONFIG_PORT_KEY);
    if (rawPort != 0)
    {
        port = (int)rawPort;
        corto_info("TCP Server Port [%i]", port);
    }
    else
    {
        corto_error("Failed to configure port property [%s].",
                    CONFIG_PORT_KEY);
        return false;
    }

    const char * serverIp = json_object_get_string(pFileObj, CONFIG_IP_KEY);
    if (serverIp != nullptr)
    {
        ip.assign(serverIp);
        corto_info("TCP Server IP [%s]", ip.c_str());
    }
    else
    {
        corto_error("Failed to configure server ip property [%s].",
                    CONFIG_IP_KEY);
        return false;
    }

    JSON_VALUE_FREE(pFile);

    return true;
}

void Execute()
{
    m_running = true;

    while (m_stopRequest == false)
    {
        char buffer[4096];
        int valRead = read(m_socket , buffer, 4096);
        if (valRead != -1)
        {
            corto_info("Data Update:\n%s\n",buffer );
        }
        else
        {
            corto_error("Failed to read.");
        }
    }

    /// Close Thread
    m_running = false;
    // if the mutex is locked, the thread is just bout to join
    if (m_threadMutex.try_lock() == true)
    {
        m_thread.detach();
    }
}

bool Start()
{
    m_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (m_socket < 0)
    {
        corto_error("Failed to create socket connection.");
        return false;
    }

    memset(&m_serverAddress, '0', sizeof(m_serverAddress));

    m_serverAddress.sin_family = AF_INET;
    m_serverAddress.sin_port = htons(m_port);

    // Convert IPv4 and IPv6 addresses from text to binary form
    if(inet_pton(AF_INET, m_ip.c_str(), &m_serverAddress.sin_addr) <= 0)
    {
        corto_error("Invalid address | Address not supported");
        return false;
    }

    int retConnect = connect(m_socket, (struct sockaddr *)&m_serverAddress, sizeof(m_serverAddress));
    if (retConnect < 0)
    {
        corto_error("TCP Connection Failed");
        return false;
    }

    if (m_running == false)
    {
        m_thread = std::thread(Execute);
    }
    else
    {
        corto_error("Cannot start already running thread.");
        return false;
    }

    return true;
}

int Run()
{
    bool done = false;

    while (done == false)
    {
        std::cout << "Sensor TCP Server." << std::endl;
        std::cout << "q. Exit" << std::endl;

        std::string choice = "";
        std::cin >> choice;
        if (choice.compare("q") == 0)
        {
            return 0;
        }
        else
        {
            std::cout << "Did not recognize input!" << std::endl;
        }
    }

    std::cout << "Finished!" << std::endl;

    return 0;
}

bool GetSocketOptError(int fd)
{
   int err = 0;

   socklen_t len = sizeof(err);

   if (getsockopt(fd, SOL_SOCKET, SO_ERROR, (char *)&err, &len) -1)
   {
       corto_error("Failed to get socket option errors.");
       return false;
   }

   return true;
}

void Destroy()
{
    m_stopRequest = true;

    //Allow thread to finish processing before join.
    while (m_running == true)
    {
        std::this_thread::sleep_for(std::chrono::microseconds(DEFAULT_SLEEP_US));
    }
    if ((m_threadMutex.try_lock() == true) && (m_thread.joinable() == true))
    {
        m_thread.join();
    }

    GetSocketOptError(m_socket);

    /// Terminate reliable connection
    if (shutdown(m_socket, SHUT_RDWR) < 0)
    {
        if (errno != ENOTCONN && errno != EINVAL) // SGI causes EINVAL
        {
            corto_error("Failed to shutdown client socket");
            return;
        }
    }

    /// Close Socket
    if (close(m_socket) < 0)
    {
        corto_error("Failed to close client socket");
    }
}

int clientMain(int argc, char *argv[])
{
    if (argc < 2)
    {
        corto_error("No configuration file specified.");
        corto_info("Usage: ./client <config.json>");
        return 1;
    }

    std::string configFile(argv[1]);

    if (UpdateConfig(configFile, m_port, m_ip) == false)
    {
        corto_error("Failed to configure.");
        return 1;
    }

    if (Start() == false)
    {
        corto_error("Failed to start client.");
        return 1;
    }

    int retVal = Run();

    Destroy();

    return retVal;
}
