# Ambyint Interview Project

## TCP Sensor Data Server

There is a file provided that represents sensor data coming into the system.
You must read this data as quickly as possible. Every 150ms you must produce a
value that accurately represents the data you have read for the previous 150ms
(i.e. you will produce 1 value every 150ms). At the same time, you must provide
a TCP server that multiple clients can connect to and these clients must be able
to connect at any time (regardless of whether you have data to read or not).
Every client that connects should get the entire collection of the records
produced every 150ms. One important factor is that this program will be
running in production so take all the steps you would normally take to
ensure the quality of your code.

Data file to download:
https://drive.google.com/file/d/0B6dDqj2bSpENeGMtdDdYbGFzY1U/view?usp=sharing


## Running

**Dependencies**

The solution runs on Linux utilizing Corto (www.corto.io), an open-source IOT
framework. Ensure the following Linux (Ubuntu 14.04) dependencies have been
installed:

* libffi-dev
* rake
* libxml2-dev
* bison
* flex
* libcurl4-openssl-dev

Corto can be installed by running the `build.sh` script, which checks out the
latest repos from GitHub. Onced the repos have been cloned, the script will build
and install the framework to `/usr/local/x`.

**Building Task data_server**

From the root project directory, simply type `corto install`.

It may be necessary to update the permissions of the data file using the
standard `config.json` configuration file. Update `config.json` permissions:
`sudo chmod a+r /usr/local/etc/corto/1.2/data_server/data`

**Running Data Server**

The application can be run from the local build directory or from the install
location:
* local: `./data_server/data_server config.json`
* install dir: `/usr/local/bin/cortobin/1.2/data_server/data_server config.json`

**Running Client**

The application can be run from the local build directory or from the install
location:
* local: `./client/client config.json`
* install dir: `/usr/local/bin/cortobin/1.2/client/client config.json`

## External Libraries

**Parson**
Lightweight JSON library written in C.
https://github.com/kgabis/parson

Utilized for runtime configuration
