#ifndef __DATA_SERVER_TCP_SERVER_H__
#define __DATA_SERVER_TCP_SERVER_H__

#include <corto/corto.h>
#include <include/task_base.h>
#include <include/sensor_data.h>
#include <string>
#include <mutex>
#include <thread>
#include <signal.h>

/// Socket headers
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>


class CTcpServer : public CTaskBase
{
public:
    typedef TSensorData<int32_t> DataType;
    typedef std::vector<int> ClientVector;

    void SensorData(DataType *pSensor);
    void Port(int port);

    bool Initialize(std::string name);
    bool Execute();
    void Destroy();

    CTcpServer();
    virtual ~CTcpServer();

private:
    bool BuildJsonSummary(std::string &jsonStr);
    void Listen();
    bool CloseSocket(int socketFd);

    std::mutex          m_mutex;

    std::mutex          m_threadMutex;
    std::thread         m_thread;
    sig_atomic_t        m_running;
    sig_atomic_t        m_stopRequest;

    ClientVector        m_clients;
    int                 m_serverFd;
    int                 m_port;
    struct sockaddr_in  m_address;

    DataType            *m_pSensor;
};

#endif
