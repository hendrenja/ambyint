/* scheduler.h
 *
 * This is the main package file. Include this file in other projects.
 * Only modify inside the header-end and body-end sections.
 */

#ifndef __DATA_SERVER_SCHEDULER_H
#define __DATA_SERVER_SCHEDULER_H

#include <corto/corto.h>
#include <oasys/core/common/common.h>
#include <include/task_base.h>

using namespace oasys::core::common;

typedef std::shared_ptr<CTaskBase> TaskPtr;

struct Task
{
    CTimer::TimerId m_id;
    TaskPtr         m_pTask;

    Task(CTimer::TimerId id, TaskPtr pTask) :
        m_id(id),
        m_pTask(pTask)
    {}
};

class CScheduler
{
public:
    /**
     * @brief Initializes Scheduler object's dependencies.
     */
    bool Initialize();

    /**
     * @brief Schedule task to execute periodically
     * @param pTask[in] Shared Pointer to target task to be scheduled.
     * @param frequency[in] Periodic timing (milliseconds) to execute target task
     * @return TRUE if successfully added. FALSE if task could not be added.
     */
    bool AddTaskSeconds(TaskPtr pTask, uint64_t frequency);

    /**
     * @brief Schedule task to execute periodically
     * @param pTask[in] Shared Pointer to target task to be scheduled.
     * @param frequency[in] Periodic timing (milliseconds) to execute target task
     * @return TRUE if successfully added. FALSE if task could not be added.
     */
    bool AddTaskMilliseconds(TaskPtr pTask, uint64_t frequency);

    /**
     * @brief Schedule task to execute periodically
     * @param pTask[in] Shared Pointer to target task to be scheduled.
     * @param frequency[in] Periodic timing (microseconds) to execute target task
     * @return TRUE if successfully added. FALSE if task could not be added.
     */
    bool AddTaskMicroseconds(TaskPtr pTask, uint64_t frequency);

    /**
     * @brief Schedule task to execute periodically
     * @param pTask[in] Pointer to target task to be scheduled.
     * @param frequency[in] Periodic cycle to execute target task
     * @return TRUE if successfully added. FALSE if task could not be added.
     */
    bool RemoveTask(std::string name);

    CScheduler();
    virtual ~CScheduler();

private:
    typedef std::map<std::string, Task> TaskMap;

    TaskMap     m_taskMap;
    CTimer      *m_pTimer;
};

#endif
