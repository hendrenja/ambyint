#ifndef __DATA_SERVER_SENSOR_DATA_H__
#define __DATA_SERVER_SENSOR_DATA_H__

#include <vector>
#include <mutex>
#include <fstream>

template<typename TData>
class TSensorData
{
public:
    typedef std::vector<TData> SensorDataVector;

    TData ReadData(void);
    SensorDataVector ReadDataCollection(void);

    bool Initialize(std::string &dataFile);
    bool Execute();

    TSensorData();
    virtual ~TSensorData();

private:
    void UpdateData(TData &data);

    std::mutex          m_mutex;

    SensorDataVector    m_vector;

    TData               m_average;
    uint64_t            m_iteration;

    std::ifstream       *m_pFile;
};

#include <include/sensor_data.cpp>
#endif
