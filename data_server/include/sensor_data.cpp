#include <thread>

typedef std::lock_guard<std::mutex> LockGuard;

template <typename TData>
TData TSensorData<TData>::ReadData(void)
{
    LockGuard lock(m_mutex);

    m_vector.push_back(m_average);

    /// Reset moving average
    m_iteration = 0;
    TData average = m_average;
    m_average = 0;

    return average;
}

template <typename TData>
typename TSensorData<TData>::SensorDataVector TSensorData<TData>::ReadDataCollection(void)
{
    LockGuard lock(m_mutex);

    return m_vector;
}

template <typename TData>
bool TSensorData<TData>::Initialize(std::string &dataFile)
{
    if (m_pFile == nullptr)
    {
        m_pFile = new std::ifstream();
    }

    m_pFile->open(dataFile.c_str());

    if (m_pFile->is_open() == false)
    {
        corto_error("Failed to open file [%s]", dataFile.c_str());
        if (m_pFile != nullptr)
        {
            delete m_pFile;
            m_pFile = nullptr;
        }
        return false;
    }

    return true;
}

template <typename TData>
bool TSensorData<TData>::Execute()
{
    if (m_pFile == nullptr)
    {
        corto_error("Cannot execute sensor data - Invalid file [NULL]");
        return false;
    }

    std::string line;
    int it = 0;
    while (std::getline(*m_pFile, line))
    {
        // Sleep after 10 reads to prevent starving other threads.
        if (it > 10)
        {
            std::this_thread::sleep_for(std::chrono::microseconds(2));
            it = 0;
        }

        LockGuard lock(m_mutex);

        float dataFloat = atof(line.c_str());
        if (dataFloat != NAN)
        {
            TData data = (TData)atoi(line.c_str());
            UpdateData(data);
        }
        else
        {
            corto_error("Failed to convert sensor data [%s]", line.c_str());
        }

        it++;
    }

    m_pFile->close();

    return true;
}

template <typename TData>
TSensorData<TData>::TSensorData() :
    m_average(0),
    m_iteration(0),
    m_pFile(nullptr)
{

}

template <typename TData>
TSensorData<TData>::~TSensorData()
{
    if (m_pFile != nullptr)
    {
        delete m_pFile;
        m_pFile = nullptr;
    }
}

template <typename TData>
void TSensorData<TData>::UpdateData(TData &data)
{
    /// Update Moving Average
    TData total = (m_average * m_iteration);
    total += data;
    m_iteration++;
    m_average = total / m_iteration;
}
