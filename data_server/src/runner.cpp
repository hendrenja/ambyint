#include <include/data_server.h>
#include <include/tcp_server.h>
#include <include/scheduler.h>
#include <iostream>

typedef TSensorData<int32_t> SensorData;
typedef std::shared_ptr<CTcpServer> TcpServer;

const char * CONFIG_DATA_FILE_KEY = "data_file";
const char * CONFIG_READ_RATE_KEY = "frequency";
const char * CONFIG_READ_RATE_UNIT_KEY = "unit";
const char * CONFIG_PORT_KEY = "port";

const std::string TCP_SERVER_NAME = "TCP_SERVER";

/// Members
TcpServer       m_pTcpServer;
CScheduler      m_scheduler;

/// Config Members
std::string     m_dataFile;
std::string     m_frequencyUnit;
uint64_t        m_frequency;
int             m_port;

#define JSON_VALUE_FREE(ptr) if (ptr != nullptr) { json_value_free(ptr); ptr = nullptr;}
#define JSON_VERIFY_PTR(ptr, name) if (ptr == nullptr) { \
            corto_error("Failed JSON access - Invalid pointer [%s]", name); return false;}

bool UpdateConfig(std::string &configFile,
                  std::string &dataFile,
                  std::string &frequencyUnit,
                  uint64_t &frequency,
                  int &port)
{
    JSON_Value *pFile = json_parse_file(configFile.c_str());
    JSON_VERIFY_PTR(pFile, "pFile");

    JSON_Object *pFileObj = json_value_get_object(pFile);
    JSON_VERIFY_PTR(pFileObj, "pFileObj");

    const char * configFileName = json_object_get_string(pFileObj, CONFIG_DATA_FILE_KEY);
    if (configFileName != nullptr)
    {
        dataFile.assign(configFileName);
        corto_info("Sensor Data File [%s]", dataFile.c_str());
    }
    else
    {
        corto_error("Failed to configure sensor data file name property [%s].",
                    CONFIG_READ_RATE_KEY);
        return false;
    }

    double rawFrequency = json_object_get_number(pFileObj, CONFIG_READ_RATE_KEY);
    if (rawFrequency != 0)
    {
        frequency = (uint64_t)rawFrequency;
        corto_info("Read Frequency [%i]", frequency);
    }
    else
    {
        corto_error("Failed to configure read frequency property [%s].",
                    CONFIG_READ_RATE_KEY);
        return false;
    }

    const char * frequencyUnitStr = json_object_get_string(pFileObj, CONFIG_READ_RATE_UNIT_KEY);
    if (frequencyUnitStr != nullptr)
    {
        frequencyUnit.assign(frequencyUnitStr);
        corto_info("Read Frequency Unit [%s]", frequencyUnit.c_str());
    }
    else
    {
        corto_error("Failed to configure read frequency unitproperty [%s].",
                    CONFIG_READ_RATE_UNIT_KEY);
        return false;
    }

    double rawPort = json_object_get_number(pFileObj, CONFIG_PORT_KEY);
    if (rawPort != 0)
    {
        port = (int)rawPort;
        corto_info("TCP Server Port [%i]", port);
    }
    else
    {
        corto_error("Failed to configure port property [%s].",
                    CONFIG_PORT_KEY);
        return false;
    }

    JSON_VALUE_FREE(pFile);

    return true;
}

bool Schedule(TaskPtr pTask,
              std::string &frequencyUnit,
              uint64_t frequency)
{
    if (frequencyUnit.compare("s") == 0)
    {
        m_scheduler.AddTaskSeconds(pTask, frequency);
    }
    else if (frequencyUnit.compare("ms") == 0)
    {
        m_scheduler.AddTaskMilliseconds(pTask, frequency);
    }
    else if (frequencyUnit.compare("us") == 0)
    {
        m_scheduler.AddTaskMicroseconds(pTask, frequency);
    }
    else
    {
        corto_error("Failed to schedule task. Unknown frequency unit.");
        corto_info("Valid Options: Seconds [s] | Milliseconds [ms] | Microseconds [us]")
    }

    return false;
}

int Configure(std::string &configFile)
{
    /// Update Configuration
    if (UpdateConfig(configFile, m_dataFile, m_frequencyUnit, m_frequency, m_port) == false)
    {
        corto_error("Failed to configure.");
        return 1;
    }

    return 0;
}

int Execute()
{
    SensorData sensor;
    sensor.Initialize(m_dataFile);

    m_pTcpServer->SensorData(&sensor);

    sensor.Execute();

    m_pTcpServer->SensorData(nullptr);

    return 0;
}

int Run()
{
    bool done = false;

    while (done == false)
    {
        std::cout << "Sensor TCP Server." << std::endl;
        std::cout << "1. Run Sensor Server" << std::endl;
        std::cout << "q. Exit" << std::endl;

        std::string choice = "";
        std::cin >> choice;
        if (choice.compare("q") == 0)
        {
            return 0;
        }
        else
        {
            switch (atoi(choice.c_str()))
            {
                case 1:
                {
                    int ret = Execute();
                    if (ret != 0)
                    {
                        return ret;
                    }
                    break;
                }
                default:
                {
                    std::cout << "Did not recognize input!" << std::endl;
                    break;
                }
            }
        }
    }

    std::cout << "Finished!" << std::endl;

    return 0;
}

int data_serverMain(int argc, char *argv[])
{
    if (argc < 2)
    {
        corto_error("No configuration file specified.");
        corto_info("Usage: ./data_server <config.json>");
        return 1;
    }

    std::string configFile(argv[1]);
    if (Configure(configFile) != 0)
    {
        corto_error("Failed to configure data server.");
        return 1;
    }

    m_pTcpServer = std::make_shared<CTcpServer>();
    if (m_scheduler.Initialize() == false)
    {
        corto_error("Failed to Initialize Data Server scheduler.");
        return 1;
    }

    m_pTcpServer->Port(m_port);
    if (m_pTcpServer->Initialize(TCP_SERVER_NAME) == false)
    {
        m_pTcpServer.reset();
        corto_error("Failed to Initialize TCP Server.")
        return 1;
    }

    Schedule(m_pTcpServer, m_frequencyUnit, m_frequency);

    int retVal = Run();

    m_pTcpServer.reset();
    m_scheduler.RemoveTask("TCP_SERVER");

    return retVal;
}
