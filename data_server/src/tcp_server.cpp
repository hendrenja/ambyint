#include <include/tcp_server.h>
#include <corto/fmt/json/parson.h>

typedef std::lock_guard<std::mutex> LockGuard;
typedef std::unique_lock<std::mutex> UniqueLock;

const uint64_t DEFAULT_SLEEP_US = 20;

void CTcpServer::SensorData(DataType *pSensor)
{
    LockGuard lock(m_mutex);

    m_pSensor = pSensor;
}

void CTcpServer::Port(int port)
{
    LockGuard lock(m_mutex);

    m_port = port;
}

bool CTcpServer::Initialize(std::string name)
{
    LockGuard lock(m_mutex);

    Name(name);

    if (m_port == 0)
    {
        corto_error("Cannot Initialize [%s] - Port undefined.", name.c_str());
        return false;
    }

    int opt = 1;

    // Creating socket file descriptor
    if ((m_serverFd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        corto_error("TCP Server socket setup failed");
        return false;
    }

    // Forcefully attaching socket to the port
    if (setsockopt(m_serverFd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
                                                  &opt, sizeof(opt)) != 0)
    {
        corto_error("Failed to setup socket descriptor for port [%i]", m_port);
        return false;
    }
    m_address.sin_family = AF_INET;
    m_address.sin_addr.s_addr = INADDR_ANY;
    m_address.sin_port = htons(m_port);

    // Forcefully attaching socket to the port 8080
    if (bind(m_serverFd, (struct sockaddr *)&m_address,
                                 sizeof(m_address)) < 0)
    {
        corto_error("Failed to bind to port [%i]", m_port);
        return false;
    }

    m_thread = std::thread(&CTcpServer::Listen, this);

    return true;
}


bool CTcpServer::Execute()
{
    LockGuard lock(m_mutex);

    if (m_pSensor == nullptr)
    {
        // corto_error("Sensor has not been Initialized in TCP Server.");
        return false;
    }

    corto_info("Data Server Reading [%i]", m_pSensor->ReadData());

    std::string jsonData;
    if (BuildJsonSummary(jsonData) == false)
    {
        corto_error("Failed to build JSON data - cannot publish samples.");
        return false;
    }

    ClientVector deadClients;

    for (auto it = m_clients.begin(); it != m_clients.end(); it++)
    {
        const char * json = jsonData.c_str();
        if (send(*it , json , (strlen(json)+1), MSG_NOSIGNAL) == -1)
        {
            deadClients.push_back(*it);
        }
    }

    if (deadClients.empty() == false)
    {
        for (auto it = deadClients.begin(); it != deadClients.end(); it++)
        {
            for (auto client = m_clients.begin(); client != m_clients.end(); client++)
            {
                if (*it == *client)
                {
                    CloseSocket(*it);
                    m_clients.erase(client);
                    break;
                }
            }
        }
    }
    deadClients.clear();

    return true;
}

void CTcpServer::Destroy()
{
    m_stopRequest = true;

    //Allow thread to finish processing before join.
    while (m_running == true)
    {
        std::this_thread::sleep_for(std::chrono::microseconds(DEFAULT_SLEEP_US));
    }
    if ((m_threadMutex.try_lock() == true) && (m_thread.joinable() == true))
    {
        m_thread.join();
    }
}

CTcpServer::CTcpServer() :
    m_running(0),
    m_stopRequest(0),
    m_port(0),
    m_pSensor(nullptr)
{

}

CTcpServer::~CTcpServer()
{
}

bool CTcpServer::BuildJsonSummary(std::string &jsonStr)
{
    DataType::SensorDataVector data = m_pSensor->ReadDataCollection();

    JSON_Value *pValue = json_value_init_object();
    JSON_Object *pData = json_value_get_object(pValue);

    for (size_t it = 0; it < data.size(); it++)
    {
        char index[255];
        sprintf(index, "%li", it);
        if (json_object_set_number(pData, index, (double)data[it]) != 0)
        {
            corto_error("Failed to serialize [%i] = [%i]", it, data[it]);
            return false;
        }
    }

    char * serialized = json_serialize_to_string(pValue);
    jsonStr.assign(serialized);

    json_free_serialized_string(serialized);
    json_value_free(pValue);

    return true;
}

void CTcpServer::Listen()
{
    m_running = true;

    if (listen(m_serverFd, 10) < 0)
    {
        corto_error("Failed to listen on port [%i]", m_port);
        throw std::exception();
    }

    while (m_stopRequest == false)
    {

        struct sockaddr_in clientAddr;
        int addrlen = sizeof(clientAddr);
        int newClient = accept(m_serverFd, (struct sockaddr *)&m_address, (socklen_t*)&addrlen);

        if (newClient < 0)
        {
            corto_error("TCP Server failed to accept new client.");
            throw std::exception();
        }

        corto_info("New client connection file descriptor [%i]", newClient);

        LockGuard lock(m_mutex);
        m_clients.push_back(newClient);

        std::this_thread::sleep_for(std::chrono::microseconds(DEFAULT_SLEEP_US));
    }

    m_running = false;

    // if the mutex is locked, the thread is just about to join
    if (m_threadMutex.try_lock() == true)
    {
        m_thread.detach();
    }
}

bool GetSocketOptError(int fd)
{
   int err = 0;

   socklen_t len = sizeof(err);

   if (getsockopt(fd, SOL_SOCKET, SO_ERROR, (char *)&err, &len) == -1)
   {
       corto_error("Failed to get socket option errors.");
       return false;
   }

   return true;
}

bool CTcpServer::CloseSocket(int socketFd)
{
    corto_info("Closing socket [%i]", socketFd);

    GetSocketOptError(socketFd);

    /// Terminate reliable connection
    if (shutdown(socketFd, SHUT_RDWR) < 0)
    {
        if ((errno != ENOTCONN) && (errno != EINVAL))
        {
            // SGI causes EINVAL
            corto_error("Failed to shutdown socket [%i]", socketFd);
            return false;
        }
    }

    /// Close Socket
    if (close(socketFd) < 0)
    {
        corto_error("Failed to close socket [%i]", socketFd);
        return false;
    }

    return true;
}
