#include "include/scheduler.h"

typedef std::pair<std::string, Task> TaskMapPair;

bool CScheduler::Initialize()
{
    if (m_pTimer == nullptr)
    {
        m_pTimer = new CTimer();
    }

    return true;
}

bool CScheduler::AddTaskSeconds(TaskPtr pTask, uint64_t frequency)
{
    return AddTaskMicroseconds(pTask, (frequency * 1000 * 1000));
}

bool CScheduler::AddTaskMilliseconds(TaskPtr pTask, uint64_t frequency)
{
    return AddTaskMicroseconds(pTask, (frequency * 1000));
}

bool CScheduler::AddTaskMicroseconds(TaskPtr pTask, uint64_t frequency)
{
    if (pTask.get() == nullptr)
    {
        corto_error("Cannot schedule task. [Task is NULL]");
        return false;
    }

    if (pTask->Name().empty() == true)
    {
        corto_error("Cannot schedule task. [Name is empty]");
        return false;
    }

    auto it = m_taskMap.find(pTask->Name());

    if (it != m_taskMap.end())
    {
        corto_info("Cannot schedule task. Task [%s] already scheduled.",
                    it->first.c_str());
        return false;
    }

    CTimer::CallbackType callback = std::bind(&CTaskBase::Execute,
                                              pTask);
    CTimer::TimerId id = m_pTimer->Create(frequency, frequency, callback);

    struct Task task(id, pTask);

    TaskMapPair pair(pTask->Name(), task);
    m_taskMap.insert(pair);

    return true;
}

bool CScheduler::RemoveTask(std::string name)
{
    auto it = m_taskMap.find(name);
    if (it == m_taskMap.end())
    {
        corto_error("Failed to Remove Task. Task [%s] not found.", name.c_str());
        return false;
    }

    struct Task task = it->second;
    if (m_pTimer->DestroyWait(task.m_id) == false)
    {
        corto_error("Failed to Remove Task. Task [%s] could not be removed from timer.",
                    name.c_str());
        return false;
    }

    m_taskMap.erase(it);

    return true;
}

CScheduler::CScheduler() :
    m_pTimer(nullptr)
{
}

CScheduler::~CScheduler()
{
    if (m_pTimer != nullptr)
    {
        delete m_pTimer;
        m_pTimer = nullptr;
    }
}
