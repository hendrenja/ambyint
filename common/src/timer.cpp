#include <oasys/core/common/timer.h>
#include <math.h>

typedef std::unique_lock<std::mutex> UniqueLock;
typedef std::lock_guard<std::mutex> LockGuard;

const int64_t DEFAULT_WAIT_US = 10;

using namespace oasys::core::common;

CTimer::TimerId CTimer::Create(uint64_t when,
                               uint64_t period,
                               CTimer::CallbackType callback)
{
    LockGuard lock(m_mutex);
    if (m_running == false)
    {
        m_worker = std::thread(&CTimer::Monitor, this);
        while (m_running == false)
        {
            std::this_thread::sleep_for(std::chrono::microseconds(DEFAULT_WAIT_US));
        }
    }

    Timestamp start = Clock::now() + Period(when);

    TimerInstance *pInstance = new TimerInstance(m_nextId,
                                                 start,
                                                 Period(period),
                                                 std::move(callback));
    m_nextId++;
    m_timerMap.insert(std::make_pair(pInstance->m_id, pInstance));
    m_queue.insert(pInstance);
    m_wakeUp.notify_all();

    return pInstance->m_id;
}

bool CTimer::Destroy(CTimer::TimerId id)
{
    bool retVal = false;

    LockGuard lock(m_mutex);

    auto it = m_timerMap.find(id);
    if (it != m_timerMap.end())
    {
        // A callback is in progress for this Instance,
        // so flag it for deletion in the worker
        if (it->second->m_active == true)
        {
            it->second->m_delete = true;
        }
        else
        {
            TimerInstance *pInstance = it->second;
            m_queue.erase(it->second);
            if (pInstance != nullptr)
            {
                delete pInstance;
            }
            m_timerMap.erase(it);
        }
        retVal = true;
    }
    m_wakeUp.notify_all();

    return retVal;
}

bool CTimer::DestroyWait(CTimer::TimerId id)
{
    bool retVal = false;
    bool wait = false;
    UniqueLock lock(m_mutex);

    auto it = m_timerMap.find(id);
    if (it != m_timerMap.end())
    {
        // The instance callback is currently executing. Flag the instance to
        // be deleted after the callback returns.
        if (it->second->m_active == true)
        {
            wait = true;
            it->second->m_delete = true;
            it->second->m_pWaitFlag = &wait;
        }
        else
        {
            TimerInstance *pInstance = it->second;
            m_queue.erase(it->second);
            if (pInstance != nullptr)
            {
                delete pInstance;
            }
            m_timerMap.erase(it);
        }
        retVal = true;
    }

    if (wait == true)
    {
        while (wait == true)
        {
            m_wakeUp.wait(lock);
        }
    }
    else
    {
        m_wakeUp.notify_all();
    }

    return retVal;
}

bool CTimer::Exists(CTimer::TimerId id)
{
    bool retVal = false;

    UniqueLock lock(m_mutex);

    auto it = m_timerMap.find(id);
    if (it != m_timerMap.end())
    {
        retVal = true;
    }

    return retVal;
}

bool CTimer::Running()
{
    bool retVal = false;

    UniqueLock lock(m_mutex);

    retVal = m_running;

    return retVal;
}

double CTimer::GetDuration(CTimer::TimerId id)
{
    double duration = NAN;

    UniqueLock lock(m_mutex);

    auto it = m_timerMap.find(id);
    if (it != m_timerMap.end())
    {
        auto durationUs = std::chrono::duration_cast<std::chrono::microseconds>(it->second->m_duration);
        duration = durationUs.count();
    }

    return duration;
}

CTimer::CTimer() :
    m_nextId(1),
    m_done(false),
    m_running(false)
{

}

CTimer::~CTimer()
{
    UniqueLock lock(m_mutex);

    m_done = true;
    m_wakeUp.notify_all();
    lock.unlock();
    if (m_running == true)
    {
        if ((m_workerMutex.try_lock() == true) && (m_worker.joinable() == true))
        {
            m_worker.join();
        }
    }

    m_queue.clear();

    for (auto iter = m_timerMap.begin(); iter != m_timerMap.end(); iter++)
    {
        TimerInstance *pInstace = iter->second;
        delete pInstace;
    }

    m_timerMap.clear();
}


void CTimer::Monitor()
{
    m_running = true;

    UniqueLock lock(m_mutex);

    while (m_done == false)
    {
        if (m_queue.empty() == true)
        {
            // Waiting for work!
            m_wakeUp.wait(lock);
        }
        else
        {
            auto firstInstance = m_queue.begin();

            TimerInstance *pInstance = *firstInstance;
            auto start = Clock::now();
            if (start >= pInstance->m_next)
            {
                m_queue.erase(firstInstance);

                // Mark instance as active to handle destory race condition.
                pInstance->m_active = true;

                start = Clock::now();
                // Call the callback
                lock.unlock();
                pInstance->m_callback();
                lock.lock();
                auto finish = Clock::now();
                pInstance->m_duration = finish - start;
                if (m_done == true)
                {
                    break;
                }
                else if (pInstance->m_delete == true)
                {
                    // Lazy delete for Destory called while Instance (Event)
                    // callback was in progress. The delete flag marks the instance
                    // to be deleted after the callback has completed. This
                    // ordering prevents a race condition while the thread is
                    // not holding the lock during callback execution.
                    m_timerMap.erase(pInstance->m_id);
                    if (pInstance->m_pWaitFlag != nullptr)
                    {
                        *pInstance->m_pWaitFlag = false;
                        m_wakeUp.notify_all();
                    }
                    delete pInstance;
                }
                else
                {
                    // If it is periodic, schedule a new one
                    if (pInstance->m_period.count() > 0)
                    {
                        if (pInstance->m_duration > (pInstance->m_period))
                        {
                            while (finish > pInstance->m_next)
                            {
                                pInstance->m_next = pInstance->m_next + pInstance->m_period;
                            }
                        }
                        else
                        {
                            pInstance->m_next = pInstance->m_next + pInstance->m_period;
                        }
                        m_queue.insert(pInstance);
                        pInstance->m_active = false;
                    }
                    else
                    {
                        m_timerMap.erase(pInstance->m_id);
                        delete pInstance;
                    }
                }
            }
            else
            {
                // Wait until the timer is ready or a timer creation notifies
                m_wakeUp.wait_until(lock, pInstance->m_next);
            }
        }
    }
    m_running = false;
    // Thread is attempting to join when the the mutex is locked
    if (m_workerMutex.try_lock() == true)
    {
        m_worker.detach();
    }
}
